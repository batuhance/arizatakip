package com.batuhan.arizakesintitakip;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by İsmailBatuhan on 24.11.2015.
 */
public class WaterFailure extends AsyncTask<Void, Void, Void> {
    ArrayList<String> dateList,areaList,timeList,detailList;

    // URL Address
    String url = "http://www.iski.gov.tr/web/arizaKesinti.aspx";

    ProgressDialog mProgressDialog;
    Dialog mDetailDialog;

    private Activity activity;

    public WaterFailure(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setTitle("Lütfen bekleyiniz..");
        mProgressDialog.setMessage("Etkilenen bölgeler getiriliyor.");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            // Connect to the web site
            Document document = Jsoup.connect(url).get();
            // Using Elements to get the class data
            Elements summary = document.select("td[style=\"font-weight:normal\"]");
            dateList = new ArrayList<String>();
            areaList = new ArrayList<String>();
            timeList = new ArrayList<String>();
            detailList = new ArrayList<String>();
            int i=0;
            for(Element sum : summary) {
                if(i%5 == 0) {
                    dateList.add(sum.text());
                }
                else if(i%5 == 1){
                    areaList.add(sum.text());
                }
                else if(i%5 == 4){
                    timeList.add(sum.text());
                }
                else if(i%5 == 3){
                    detailList.add(sum.text().toString().replaceAll(",", "\n"));
                }
                i++;
            }

            /*Elements details = document.select("div.subAffectedAreas:containsOwn(MERKEZ)");
            detailList = new ArrayList<String>();
            for(Element detail : details) {
                detailList.add(detail.text().toString().substring(0, detail.text().toString().length() - 1).replaceAll(", ", "\n").replaceAll("MERKEZ - MERKEZ -",""));
            }*/
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        mProgressDialog.dismiss();
        // Set downloaded image into ImageView
        ListView list = (ListView) activity.findViewById(R.id.listView);
        final List<MyStringPair> myStringPairList = MyStringPair.makeData(dateList,areaList,timeList);
        MyStringPairAdapter adapter = new MyStringPairAdapter(activity, myStringPairList);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDetailDialog = new Dialog(activity);
                mDetailDialog.setContentView(R.layout.dialog);
                mDetailDialog.setTitle("Etkilenen Bölgeler");
                //mDetailDialog.setMessage(detailList.get(position).toString());
                TextView message = (TextView) mDetailDialog.findViewById(R.id.dialogView);
                message.setText(detailList.get(position).toString());
                mDetailDialog.show();
            }
        });
    }
}
