package com.batuhan.arizakesintitakip;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by İsmailBatuhan on 24.11.2015.
 */
public class MyStringPair {
    private String columnOne;
    private String columnTwo;
    private String columnThree;

    public MyStringPair(String columnOne, String columnTwo, String columnThree) {
        super();
        this.columnOne = columnOne;
        this.columnTwo = columnTwo;
        this.columnThree = columnThree;
    }

    public String getColumnOne() {
        return columnOne;
    }
    public void setColumnOne(String columnOne) {
        this.columnOne = columnOne;
    }
    public String getColumnTwo() {
        return columnTwo;
    }
    public void setColumnTwo(String columnTwo) {
        this.columnTwo = columnTwo;
    }
    public String getColumnThree() {
        return columnThree;
    }
    public void columnThree(String columnThree) {
        this.columnThree = columnThree;
    }

    public static List<MyStringPair> makeData(ArrayList<String> dateList,ArrayList<String> areaList,ArrayList<String> timeList) {
        List<MyStringPair> pair = new ArrayList<MyStringPair>();
        for(int i=0; dateList.size()>i; i++) {
            pair.add(new MyStringPair(dateList.get(i).toString(),areaList.get(i).toString(),timeList.get(i).toString()));
        }
        return pair;
    }
}
