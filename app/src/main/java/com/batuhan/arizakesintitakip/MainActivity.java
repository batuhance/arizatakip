package com.batuhan.arizakesintitakip;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    Context con;
    Activity activity;

    private SwipeRefreshLayout swipeRefreshLayout;
    public String isView;

    AlertDialog.Builder mConnectionAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        con = this;
        activity = this;

        // Locate the Buttons in activity_main.xml
        Button listbutton = (Button) findViewById(R.id.listbutton);

        final LinearLayout layout = (LinearLayout) findViewById(R.id.linearLayout);
        final ListView listView = (ListView) findViewById(R.id.listView);
        final TextView lastUpdateTime = (TextView) findViewById(R.id.lastUpdateTime);

        // Capture button click
        listbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                // Execute Logo AsyncTask
                /*PowerFailure pw_failure = new PowerFailure(activity);
                pw_failure.execute();*/

                mConnectionAlert = new AlertDialog.Builder(con).setMessage("Lütfen internet bağlantınızı kontrol ediniz!");

                final CharSequence[] items = {"Ayedaş", "İSKİ"};

                AlertDialog.Builder builder = new AlertDialog.Builder(con);
                builder.setTitle("Arızasını görüntülemek istediğiniz firmayı seçin.");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            if (isNetworkAvailable(con)) {
                                PowerFailure pw_failure = new PowerFailure(activity);
                                pw_failure.execute();
                                isView = "power";
                                layout.setVisibility(View.VISIBLE);
                                listView.setVisibility(View.VISIBLE);

                                lastUpdateTime.setText("Son Güncelleme : " + Calendar.getInstance().getTime().toLocaleString());
                                lastUpdateTime.setVisibility(View.VISIBLE);
                            } else {
                                mConnectionAlert.show();
                            }
                        }
                        if (item == 1) {
                            if (isNetworkAvailable(con)) {
                                WaterFailure wt_failure = new WaterFailure(activity);
                                wt_failure.execute();
                                isView = "water";
                                layout.setVisibility(View.VISIBLE);
                                listView.setVisibility(View.VISIBLE);

                                lastUpdateTime.setText("Son Güncelleme : " + Calendar.getInstance().getTime().toLocaleString());
                                lastUpdateTime.setVisibility(View.VISIBLE);
                            } else {
                                mConnectionAlert.show();
                            }
                        }
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) activity.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isView == "water") {
                    if (isNetworkAvailable(con)) {
                        WaterFailure wt_failure = new WaterFailure(activity);
                        wt_failure.execute();
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        lastUpdateTime.setText("Son Güncelleme : " + Calendar.getInstance().getTime().toLocaleString());
                    } else {
                        mConnectionAlert.show();
                    }
                }
                else if (isView == "power") {
                    if (isNetworkAvailable(con)) {
                        PowerFailure pw_failure = new PowerFailure(activity);
                        pw_failure.execute();
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        lastUpdateTime.setText("Son Güncelleme : " + Calendar.getInstance().getTime().toLocaleString());
                    } else {
                        mConnectionAlert.show();
                    }
                }
                else {
                    Toast.makeText(getBaseContext(), "Arızasını görüntülemek istediğiniz firmayı seçin!",
                            Toast.LENGTH_SHORT).show();
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }
        });
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
